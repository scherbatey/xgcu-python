import logging

LOG_LEVEL_OPT = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'fatal': logging.FATAL
}

PACKET_BEGIN = b"\xbc\xbc"
PACKET_BEGIN_LEN = len(PACKET_BEGIN)
PACKET_END = b"\xfc\xfc"
PACKET_END_LEN = len(PACKET_END)

BROADCAST_PORT = 7000


class InvalidCommand(ValueError):
    pass


class CommandFailure(RuntimeError):
    pass