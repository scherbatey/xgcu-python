FIRMWARE_VERSIONS = {0x0101}

API_COMMANDS = {
    "":   0x01,     # Network settings (broadcast)

    "IN": 0x10,     # Settings
    "IN1": 0x11,    # Default settings

    "ST": 0x20,     # View time
    "NT": 0x21,     # Integration time
    "OM": 0x22,     # Operational mode
    "SG": 0x23,     # AD gain
    "CN": 0x25,     # DM module number for each channel
    "SF": 0x27,     # Scanning

    "EG": 0x30,     # Gain correction
    "EO": 0x31,     # Offset correction
    "EB": 0x32,     # Baseline correction
    "GC": 0x33,     # Load channel gain from flash
    "OC": 0x34,     # Reset channel gain to 1.0

    "DA": 0x40,     # Pixel binning status
    "AF": 0x41,     # Averaging filter mode
    "SU": 0x42,     # Summing filter mode

    "LM": 0x50,     # External line trigger mode
    "EL": 0x51,     # External line trigger
    "TD": 0x52,     # Line trigger fine delay
    "RD": 0x53,     # Line trigger raw delay
    "TC": 0x5a,     # Trigger stamp parity check mode

    "TP": 0x60,     # Heartbeat
    "GS": 0x62,     # X-GCU serial number
    "DS": 0x63,     # DM serial number
    "PN": 0x64,     # Total pixel number
    "IR": 0x67,     # Integration time range
    "GF": 0x68,     # X-GCU firmware version
    "DF": 0x69,     # DM firmware version
    "ED": 0x6a,     # Test pattern
    "DT": 0x6b,     # DM test/work mode
    "DP": 0x6c,     # Pixel number per DM

    "GI": 0x72,     # Voltage, temperature, humidity
    "DI": 0x73,     # DM's voltage and temperature
    "LC": 0x75,     # LED state
    "HG": 0x78,     # X-GCU hidden version number
    "HD": 0x79,     # X-DFE hidden version number
    "TI": 0x7c,     # Gain table ID
    "MT": 0x7e,     # MTU size

    "ES": 0x8d,     # Output scaling mode
    "WT": 0x8e,     # GCU working time

    "AR": 0x91,     # AD register
    "PR": 0x92,     # AD output valid bit range
    "IC": 0x93,     # AD offset correction
    "RC": 0x95,     # Reconfigure firmware of X-GCU or X-DEF
    "GR": 0x96,     # Reset Gantry
    "SI": 0x97,     # AD integration starting side
    "NC": 0x98,     # Parameters of noncontinuous mode
    "SL": 0x99,     # Sync interface test mode
    "DV": 0x9a,     # Voltage

    # TODO: check list!
}


API_COMMANDS.update([reversed(item) for item in API_COMMANDS.items()])
