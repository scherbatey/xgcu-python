FIRMWARE_VERSIONS = {0x0106}

API_COMMANDS = {
    "":   0x01,     # Network settings (broadcast)

    "IN": 0x10,     # Settings
    "IN1": 0x11,    # Default settings

    "ST": 0x20,     # View time
    "NT": 0x21,     # Integration time
    "OM": 0x22,     # Operational mode
    "SG": 0x23,     # AD gain
    "CN": 0x25,     # DM module number for each channel
    "SF": 0x27,     # Scanning

    "EL": 0x51,     # External encoder trigger

    "TP": 0x60,     # Heartbeat
    "GS": 0x62,     # X-GCU serial number
    "DS": 0x63,     # DM serial number
    "PN": 0x64,     # Total pixel number
    "IR": 0x67,     # Integration time range
    "GF": 0x68,     # X-GCU firmware version
    "DF": 0x69,     # DM firmware version
    "ED": 0x6a,     # Test pattern
    "DT": 0x6b,     # DM test/work mode
    "DP": 0x6c,     # Pixel number per DM

    "AN": 0x70,     # AD number per DFE
    "GI": 0x72,     # Voltage, temperature, humidity
    "DI": 0x73,     # DM's voltage and temperature
    "LC": 0x75,     # LED state
    "TR": 0x76,     # RS485 bus terminal resistor state
    "LL": 0x77,     # LVDS loopback
    "HG": 0x78,     # X-GCU hidden version number
    "HD": 0x79,     # X-DFE hidden version number
    "PO": 0x7a,     # Pixel order mode of card
    "MT": 0x7e,     # MTU size

    "WT": 0x8e,     # GCU working time
    "EI": 0x90,     # Encoder increment counter
    "AR": 0x91,     # AD register
    "PR": 0x92,     # AD output valid bit range
    "OC": 0x93,     # AD offset correction
    "RC": 0x95,     # Reconfigure firmware of X-GCU or X-DEF
    "GR": 0x96,     # Reset Gantry
    "IS": 0x97,     # AD integration starting side

    "ES": 0xa0,     # Encoder counting status
    "EV": 0xa1,     # Encoder count per view
    "VR": 0xa2,     # View count per rotation
}


API_COMMANDS.update([reversed(item) for item in API_COMMANDS.items()])
