from collections import namedtuple, defaultdict

import numpy as np
from crccheck.crc import Crc32Mpeg2

from xgcu import PACKET_BEGIN, PACKET_END, PACKET_END_LEN, PACKET_BEGIN_LEN

import logging
logger = logging.getLogger(__name__)

crc32mpeg2 = Crc32Mpeg2()

# Header
HEADER_INDEX_CMD = 0
HEADER_INDEX_LINE_ID = HEADER_INDEX_CMD + 1
HEADER_INDEX_PACKET_ID = HEADER_INDEX_LINE_ID + 2
HEADER_INDEX_PAYLOAD_SIZE = HEADER_INDEX_PACKET_ID + 1

HEADER_SIZE = HEADER_INDEX_PAYLOAD_SIZE + 2

# Payload
PAYLOAD_INDEX_VIEW_COUNT = 0
PAYLOAD_INDEX_LINE_SIZE = PAYLOAD_INDEX_VIEW_COUNT + 4
PAYLOAD_INDEX_GANTRY_STATUS = PAYLOAD_INDEX_LINE_SIZE + 4
PAYLOAD_INDEX_ETHERNET_STATUS = PAYLOAD_INDEX_GANTRY_STATUS + 1
PAYLOAD_INDEX_VIEW_ID = PAYLOAD_INDEX_ETHERNET_STATUS + 1
PAYLOAD_INDEX_PACKET_NUM = PAYLOAD_INDEX_VIEW_ID + 2

PAYLOAD_INDEX_DM_INFO = PAYLOAD_INDEX_PACKET_NUM + 8

uint32_big = np.dtype('>u4')
uint16_big = np.dtype('>u2')


def get_packets(stream):
    while True:
        packet_begin = stream.read(PACKET_BEGIN_LEN)
        if len(packet_begin) == 0:
            break
        if packet_begin != PACKET_BEGIN:
            raise RuntimeError(f"Invalid packet start: {packet_begin}")
        _header: bytes = stream.read(HEADER_SIZE)
        if len(_header) < HEADER_SIZE:
            break
        _cmd = _header[HEADER_INDEX_CMD]
        _line_id = int.from_bytes(_header[HEADER_INDEX_LINE_ID: HEADER_INDEX_LINE_ID + 2], 'big')

        _packet_id = _header[HEADER_INDEX_PACKET_ID]
        payload_size = int.from_bytes(_header[HEADER_INDEX_PAYLOAD_SIZE: HEADER_INDEX_PAYLOAD_SIZE + 2], 'big')

        _payload = stream.read(payload_size)
        if len(_header) < HEADER_SIZE:
            break

        checksum_orig = stream.read(4)
        checksum = crc32mpeg2.calcbytes(_header + _payload)  # TODO: optimize

        if checksum != checksum_orig:
            raise RuntimeError(f"Checksum {checksum.hex()} do not match original {checksum_orig.hex()}")

        packet_end = stream.read(PACKET_END_LEN)

        if packet_end != PACKET_END:
            raise RuntimeError("Invalid packet end")

        yield _header, _payload, _cmd, _line_id, _packet_id


LineMetadata = namedtuple(
    "LineMetadata",
    "cmd line_id view_count line_size gantry_status ethernet_status view_id packet_num"
)


def get_lines(packet_generator):
    current_line_id: int = None
    line_bytes_read: int = None
    packet_num: int = None
    current_packet_id: int = None
    skip_rest_of_line: bool = None
    metadata: LineMetadata = None
    line_data = bytearray()
    for header, payload, cmd, line_id, packet_id in get_packets(packet_generator):
        if packet_id == 0:
            if line_id == current_line_id:
                logger.warning("Repeating line id: %s", line_id)

            if line_bytes_read:
                yield line_data[:line_bytes_read], metadata

            view_count = int.from_bytes(payload[PAYLOAD_INDEX_VIEW_COUNT: PAYLOAD_INDEX_VIEW_COUNT + 4], 'big')
            line_size = int.from_bytes(payload[PAYLOAD_INDEX_LINE_SIZE: PAYLOAD_INDEX_LINE_SIZE + 4], 'big')
            gantry_status = payload[PAYLOAD_INDEX_GANTRY_STATUS]
            ethernet_status = payload[PAYLOAD_INDEX_ETHERNET_STATUS]
            view_id = int.from_bytes(payload[PAYLOAD_INDEX_VIEW_ID: PAYLOAD_INDEX_VIEW_ID + 2], 'big')
            packet_num = payload[PAYLOAD_INDEX_PACKET_NUM]
            metadata = LineMetadata(cmd, line_id, view_count, line_size, gantry_status, ethernet_status, view_id, packet_num)

            current_line_id = line_id
            line_bytes_read = 0
            skip_rest_of_line = False

            if 0 < line_size != len(line_data):
                line_data = bytearray(line_size)
        elif current_line_id == line_id:
            if packet_id - 1 != current_packet_id:
                logger.warning("Packet with id %s is missing! Skipping the rest of line")
                skip_rest_of_line = True

            if not skip_rest_of_line:
                line_data[line_bytes_read : line_bytes_read + len(payload)] = payload
                line_bytes_read += len(payload)
                if 0 < packet_num <= packet_id:
                    logger.warning("Packet id %s greater than max of %s", packet_id, packet_num - 1)
        else:
            logging.error("Skipping incomplete line")

        current_packet_id = packet_id

    if line_bytes_read:
        yield line_data[:line_bytes_read], metadata


class Image:
    __slots__ = ('img', 'line_number', 'metadata')

    def __init__(self, max_line_count: int = 0, line_size: int = 0, **kwargs):
        self.img = np.ndarray(shape=(max_line_count, line_size), dtype='uint32')
        self.line_number = 0
        self.metadata = dict(kwargs)

    def is_full(self):
        return self.line_number >= self.img.shape[0]

    def add_line(self, line):
        self.img[self.line_number] = line
        self.line_number += 1

    def trunc(self):
        pass  # self.img.resize((self.line_number, self.img.shape[1]))


def get_images(packet_generator, max_line_count: int):
    line_size: int = None
    view_id: int = None

    images = defaultdict(lambda: Image(max_line_count, line_size, view_id=view_id))

    for line_data, line_metadata in get_lines(packet_generator):
        if len(line_data) % uint16_big.itemsize:
            raise RuntimeError("Incorrect line size")
        line_size = len(line_data) // uint16_big.itemsize
        view_id = line_metadata.view_id
        img_data = images[view_id]

        if img_data.img.shape[1] != line_size:
            img_data.trunc()
            yield img_data
            del images[view_id]
            img_data = images[view_id]

        try:
            img_data.add_line(np.ndarray(shape=(line_size,), dtype=uint16_big, buffer=line_data))
        except MemoryError:
            logger.error("Memory error at line %s", line_metadata.line_id)

        if img_data.is_full():
            yield img_data
            del images[view_id]

    for img_data in images.values():
        img_data.trunc()
        yield img_data


def read_images(filename, max_line_number: int = 10000):
    with open(filename, "rb") as f:
        images = [x for x in get_images(f, max_line_number)]
    return images
