from xgcu import CommandFailure
from .commands import *


def _voltage_value(b: bytes):
    return int.from_bytes(b, 'big') * 2.048 / 2047


class XGCUDevice:
    def __init__(self, ip: str, cmd_port: int, firmware_version: [str, int] = None, timeout: float = 2):
        self.ip = ip
        self.cmd_port = cmd_port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.settimeout(timeout)

        if firmware_version in API_COMMANDS:
            self.firmware_version = firmware_version
        else:
            command = make_get_api_version_command()
            answer = do_request(command, (self.ip, self.cmd_port), self.socket)
            data_len = answer[PACKET_INDEX_DATA_LEN]
            if answer[PACKET_INDEX_ERR_CODE] != 0:
                raise CommandFailure(f"Command failed! X-GCU returned failure status "
                                     f"0x{answer[PACKET_INDEX_ERR_CODE].to_bytes(1, 'big').hex()}")
            if data_len != 2:
                raise InvalidCommand(f'Invalid data length: {data_len} instead of 2')
            self.firmware_version = int.from_bytes(answer[PACKET_INDEX_DATA:PACKET_INDEX_DATA + data_len], 'big')
        self.api_commands = API_COMMANDS[self.firmware_version]

    def __del__(self):
        self.socket.close()

    def set_scanning(self, start: bool):
        cmd = make_command(CMD_SCANNING, OPE_WRITE, data=b'\1' if start else b'\0')
        answer = do_request(cmd, (self.ip, self.cmd_port), self.socket)
        return answer[PACKET_INDEX_OPE] == 0

    def set_heartbeat(self, interval: int):
        cmd = make_command(CMD_HEARTBEAT, OPE_WRITE, data=interval.to_bytes(1, 'big'))
        answer = do_request(cmd, (self.ip, self.cmd_port), self.socket)
        return answer[PACKET_INDEX_OPE] == 0

    def get_voltage_temperature_humidity(self):
        cmd = make_command(CMD_TEMPERATURE_VOLTAGE, OPE_READ)
        answer = do_request(cmd, (self.ip, self.cmd_port), self.socket)
        return {
            "V1":       _voltage_value(answer[PACKET_INDEX_DATA +  0:PACKET_INDEX_DATA +  2]) * 24 / 1.5,
            "V2":       _voltage_value(answer[PACKET_INDEX_DATA +  2:PACKET_INDEX_DATA +  4]) * 2,
            "V3":       _voltage_value(answer[PACKET_INDEX_DATA +  4:PACKET_INDEX_DATA +  6]) * 2,
            "V4":       _voltage_value(answer[PACKET_INDEX_DATA +  6:PACKET_INDEX_DATA +  8]),
            "DB_V1":    _voltage_value(answer[PACKET_INDEX_DATA +  8:PACKET_INDEX_DATA + 10]) * 2,
            "DB_V2":    _voltage_value(answer[PACKET_INDEX_DATA + 10:PACKET_INDEX_DATA + 12]) * 16.125,
            "T":        int.from_bytes(answer[PACKET_INDEX_DATA + 12:PACKET_INDEX_DATA + 14], 'big') * 0.125,
            "H":        int.from_bytes(answer[PACKET_INDEX_DATA + 14:PACKET_INDEX_DATA + 16], 'big') * 125 / 65536 - 6,
        }

    def command(self, cmd: bytes):
        answer = do_request(cmd, (self.ip, self.cmd_port), self.socket)
        return answer

    def command_str(self, cmd_str: str):
        cmd_str = make_str_command(cmd_str, self.api_commands)
        answer = self.command(cmd_str)
        if answer[PACKET_INDEX_ERR_CODE] != 0:
            return f"[{answer[PACKET_INDEX_ERR_CODE]}]"
        data_len = answer[PACKET_INDEX_DATA_LEN]
        if data_len == 0:
            return f"[0]"
        return f"[0,{answer[PACKET_INDEX_DATA: PACKET_INDEX_DATA + data_len].hex()}]"
