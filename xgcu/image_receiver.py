import socketserver

import logging
logger = logging.getLogger(__name__)


def make_handler_class(stream, buffer_size: int = 0x400000):

    class DataFrameHandlerClass(socketserver.DatagramRequestHandler):
        file = stream
        buffer = bytearray(buffer_size)
        byte_counter = 0

        def handle(self):
            while True:
                bytes_read = self.rfile.readinto(self.buffer)
                if not bytes_read:
                    break
                if not self.byte_counter:
                    logger.info("Acquisition started!")
                type(self).byte_counter += bytes_read

                logger.debug("Read %s bytes", bytes_read)
                self.file.write(self.buffer[:bytes_read])
                logger.debug("Written %s bytes to file", bytes_read)

                # self.wfile.write(bytes(str(bytes_read), 'utf8'))
                # logger.debug("Sent response to %s", self.client_address)

    return DataFrameHandlerClass
