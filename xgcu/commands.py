import re
import socket
import time

from typing import Tuple, List
Address = Tuple[str, int]

from crccheck.crc import Crc32Mpeg2

from xgcu import PACKET_BEGIN, PACKET_END, PACKET_BEGIN_LEN, PACKET_END_LEN, InvalidCommand

API_COMMANDS = {}

from xgcu import api_tobi

API_COMMANDS["tobi"] = api_tobi.API_COMMANDS
for api_version in api_tobi.FIRMWARE_VERSIONS:
    API_COMMANDS[api_version] = api_tobi.API_COMMANDS

from xgcu import api_minx

API_COMMANDS["minx"] = api_minx.API_COMMANDS
for api_version in api_minx.FIRMWARE_VERSIONS:
    API_COMMANDS[api_version] = api_minx.API_COMMANDS

import logging
logger = logging.getLogger(__name__)

OPE_EXECUTE = 0x00
OPE_WRITE = 0x01
OPE_READ = 0x02
OPE_SAVE = 0x03
OPE_LOAD = 0x04
OPE_DEFAULT = 0x05

OPE_CODE = {
    'E': OPE_EXECUTE,
    'W': OPE_WRITE,
    'R': OPE_READ,
    'S': OPE_SAVE,
    'L': OPE_LOAD,
    'D': OPE_DEFAULT   # For use only with 0x01 broadcast command. Recovers default network settings
}

ERR_SUCCESS = 0

PACKET_INDEX_COMMAND = PACKET_BEGIN_LEN
PACKET_INDEX_OPE = PACKET_INDEX_COMMAND + 1
PACKET_INDEX_ERR_CODE = PACKET_INDEX_COMMAND + 1
PACKET_INDEX_DM_ID = PACKET_INDEX_ERR_CODE + 1
PACKET_INDEX_DATA_LEN = PACKET_INDEX_DM_ID + 1
PACKET_INDEX_DATA = PACKET_INDEX_DATA_LEN + 1

CRC_LENGTH = 4
PACKET_HEADER_LEN = PACKET_INDEX_DATA
PACKET_MIN_LENGTH = PACKET_HEADER_LEN + CRC_LENGTH + PACKET_END_LEN

CMD_NETWORK_CONFIG = 0x01
# 
# CMD_VIEW_TIME = 0x20
# CMD_CONST_INTEGRATION_TIME = 0x21
CMD_SCANNING = 0x27
CMD_HEARTBEAT = 0x60
# 
# CMD_TEST_PATTERN = 0x6a
# CMD_DM_TEST_MODE = 0x6b
# 
CMD_TEMPERATURE_VOLTAGE = 0x72



crc32mpeg2: Crc32Mpeg2 = Crc32Mpeg2()

_re_command = re.compile(
    r"\[(\w{0,3}|0X[0-9A-F]{2}),([ERWSLD]{1}),([0-9A-F]{1,2})"
    r"(?:,([0-9A-F]+))?\]"
)


def make_command(command: int, operation: int, dm_id: int = 0, data: bytes = b""):
    data_len = len(data)
    payload_end = PACKET_INDEX_DATA + data_len
    assert payload_end + CRC_LENGTH + PACKET_END_LEN == PACKET_MIN_LENGTH + data_len

    cmd = bytearray(PACKET_MIN_LENGTH + data_len)

    cmd[:PACKET_BEGIN_LEN] = PACKET_BEGIN
    cmd[PACKET_INDEX_COMMAND:PACKET_INDEX_DATA] = command, operation, dm_id, data_len
    cmd[PACKET_INDEX_DATA: payload_end] = data

    crc_bytes = crc32mpeg2.calcbytes(cmd[PACKET_INDEX_COMMAND:payload_end])
    assert len(crc_bytes) == CRC_LENGTH

    cmd[payload_end: payload_end + CRC_LENGTH] = crc_bytes
    cmd[payload_end + CRC_LENGTH:] = PACKET_END
    return cmd


COMMAND_GET_FIRMWARE_VERSION = 0x68


def make_get_api_version_command():
    return make_command(COMMAND_GET_FIRMWARE_VERSION, OPE_READ, 0)


def make_str_command(command: str, api_commands: dict):
    cmd_code, mode, dm_id, data = _re_command.match(command.upper()).groups()
    if data and len(data) % 2:
        data = '0' + data
    if cmd_code.startswith("0X"):
        cmd_code = int(cmd_code[2:], 16)
    else:
        cmd_code = api_commands[cmd_code]
    mode = OPE_CODE[mode]
    dm_id = int(dm_id, 16)
    data = bytes.fromhex(data) if data else b""
    return make_command(cmd_code, mode, dm_id, data)


CMD_CFG_FRAME = 0xf3

CFG_FRAME_PAYLOAD_ID_LEN = 4
CFG_FRAME_PAYLOAD_LEN = 512
CFG_FRAME_DATA_LEN = CFG_FRAME_PAYLOAD_ID_LEN + CFG_FRAME_PAYLOAD_LEN
PACKET_INDEX_CFG_FRAME_PAYLOAD = PACKET_INDEX_DATA + CFG_FRAME_PAYLOAD_ID_LEN
CFG_FRAME_DATA_END = PACKET_INDEX_CFG_FRAME_PAYLOAD + CFG_FRAME_PAYLOAD_LEN
CFG_FRAME_PACKET_LENGTH = CFG_FRAME_DATA_END + CRC_LENGTH + PACKET_END_LEN


def make_config_frame_commands(dm_id: int, firmware_data: bytes):
    chunk_id = 0
    chunk_start = 0
    data_end = PACKET_INDEX_CFG_FRAME_PAYLOAD + CFG_FRAME_PAYLOAD_LEN
    while True:
        cmd = bytearray(PACKET_MIN_LENGTH + CFG_FRAME_DATA_LEN)
        cmd[:PACKET_BEGIN_LEN] = PACKET_BEGIN
        # TODO: check behavior for len(data) > 0xff
        cmd[PACKET_INDEX_COMMAND:PACKET_HEADER_LEN] = CMD_CFG_FRAME, OPE_WRITE, dm_id, 0x04
        cmd[PACKET_INDEX_DATA:PACKET_INDEX_CFG_FRAME_PAYLOAD] = chunk_id.to_bytes(CFG_FRAME_PAYLOAD_ID_LEN, 'big')
        chunk_size = min(CFG_FRAME_PAYLOAD_LEN, len(firmware_data) - chunk_start)
        useful_data_end = PACKET_INDEX_CFG_FRAME_PAYLOAD + chunk_size
        cmd[PACKET_INDEX_CFG_FRAME_PAYLOAD:useful_data_end] = \
            firmware_data[chunk_start: chunk_start + chunk_size]

        if useful_data_end < data_end:
            cmd[useful_data_end: data_end] = b'\xff' * (data_end - useful_data_end)

        crc_bytes = crc32mpeg2.calcbytes(cmd[PACKET_INDEX_COMMAND:data_end])
        crc_end = data_end + CRC_LENGTH
        cmd[data_end: crc_end] = crc_bytes

        assert crc_end + PACKET_END_LEN == len(cmd)

        cmd[crc_end:] = PACKET_END

        yield cmd

        if useful_data_end < data_end:
            break
        chunk_start += CFG_FRAME_PAYLOAD_LEN
        chunk_id += 1


def get_answer(sock: socket.socket):
    while True:
        answer, host = sock.recvfrom(512)
        validate_command(answer)
        # Skip heartbeat
        if answer[PACKET_INDEX_COMMAND] != 0xff:
            break

    logger.debug("Reply from address %s: %s", host, answer.hex())

    return answer, host


def do_request(
    cmd: bytes,
    address: Address,
    sock: socket.socket,
) -> bytes:
    logger.debug("Sending: %s", cmd.hex())
    sock.sendto(cmd, address)
    answer, host = get_answer(sock)
    if answer[PACKET_INDEX_COMMAND] != cmd[PACKET_INDEX_COMMAND]:
        raise InvalidCommand(f"Received invalid answer: command IDs do not match: "
                             f"{answer[PACKET_INDEX_COMMAND]} instead of {cmd[PACKET_INDEX_COMMAND]}")
    return answer


def do_broadcast_request(
        cmd: bytes,
        address: Address,
        sock: socket.socket,
) -> List[Tuple[bytes, Address]]:
    logger.debug("Sending broadcast: %s", cmd.hex())
    sock.sendto(cmd, address)

    if not sock.gettimeout():
        return [get_answer(sock)]

    answers = []
    timeout = sock.gettimeout()
    start = time.monotonic()
    while time.monotonic() - start < timeout:
        try:
            answer, host = get_answer(sock)
            if answer[PACKET_INDEX_COMMAND] != cmd[PACKET_INDEX_COMMAND]:
                raise InvalidCommand(f"Received invalid answer: command IDs do not match: "
                                     f"{answer[PACKET_INDEX_COMMAND]} instead of {cmd[PACKET_INDEX_COMMAND]}")
            answers.append((answer, host))
        except socket.timeout:
            pass
        except InvalidCommand as e:
            logger.error(e)
    return answers


def get_network_config(broadcast_address: Address, local_address: Address = None, timeout: float = 2):
    cmd = make_command(CMD_NETWORK_CONFIG, OPE_READ)
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.settimeout(timeout)
        if local_address:
            if broadcast_address[1] == local_address[1]:
                raise ValueError(f"Local and remote port for broadcast command cannot be the same : {local_address[1]}")
            sock.bind(local_address)
        answers = do_broadcast_request(cmd, broadcast_address, sock)
        return [{
            "reply_from": host,
            "sn": str(answer[PACKET_INDEX_DATA:PACKET_INDEX_DATA + 32].strip(b"\0 "), 'ascii'),
            "ip": ".".join(str(b) for b in answer[PACKET_INDEX_DATA + 32:PACKET_INDEX_DATA + 36]),
            "mac": answer[PACKET_INDEX_DATA + 36:PACKET_INDEX_DATA + 42].hex(),
            "cmd_port": int.from_bytes(answer[PACKET_INDEX_DATA + 42:PACKET_INDEX_DATA + 44], 'big'),
            "img_port": int.from_bytes(answer[PACKET_INDEX_DATA + 44:PACKET_INDEX_DATA + 46], 'big')
        } for answer, host in answers]


def validate_command(cmd: bytes):
    if len(cmd) < PACKET_MIN_LENGTH:
        raise InvalidCommand(f"Command is too short, length: {len(cmd)} < {PACKET_MIN_LENGTH}")
    if cmd[:PACKET_INDEX_COMMAND] != PACKET_BEGIN:
        raise InvalidCommand(f"Invalid command start: {cmd[:PACKET_INDEX_COMMAND].hex()}")
    cmd_id = cmd[PACKET_INDEX_COMMAND]
    if cmd_id == CMD_CFG_FRAME and cmd[PACKET_INDEX_DATA_LEN] != 0:
        if len(cmd) != CFG_FRAME_PACKET_LENGTH:
            raise InvalidCommand(f"Config frame command has invalid size, length: "
                                 f"{len(cmd)} instead of {CFG_FRAME_PACKET_LENGTH}")
        data_end = CFG_FRAME_DATA_END
    else:
        data_end = PACKET_INDEX_DATA + cmd[PACKET_INDEX_DATA_LEN]
        if len(cmd) != data_end + CRC_LENGTH + PACKET_END_LEN:
            raise InvalidCommand(f"Command has invalid size, length: "
                                 f"{len(cmd)} < {CFG_FRAME_PACKET_LENGTH}")
    crc_bytes = crc32mpeg2.calcbytes(cmd[PACKET_INDEX_COMMAND:data_end])
    if crc_bytes != cmd[data_end:data_end + CRC_LENGTH]:
        raise InvalidCommand(f"CRC check error: actual {crc_bytes}, specified {cmd[data_end:data_end + CRC_LENGTH]}")


def parse_command(cmd: bytes):
    validate_command(cmd)
    cmd_id, ope_or_err_id, dm_id, data_size = cmd[PACKET_INDEX_COMMAND:PACKET_INDEX_DATA]
    if cmd_id == CMD_CFG_FRAME:
        data_size = CFG_FRAME_DATA_LEN
    data = cmd[PACKET_INDEX_DATA: PACKET_INDEX_DATA + data_size]
    return cmd_id, ope_or_err_id, dm_id, data
