from socketserver import DatagramRequestHandler
from threading import Thread
from time import sleep

from xgcu import BROADCAST_PORT, InvalidCommand

from .commands import CMD_CFG_FRAME, OPE_WRITE, OPE_READ, ERR_SUCCESS, \
    CFG_FRAME_PAYLOAD_ID_LEN, \
    parse_command, make_command, CFG_FRAME_DATA_LEN, PACKET_INDEX_DATA_LEN

import logging
logger = logging.getLogger(__name__)


ERR_FAIL = 0xff


def check_data_size(data: bytes, size: int):
    if len(data) != size:
        raise InvalidCommand(f'Invalid data size: {len(data)} instead of {size}')


class CommandHandler(DatagramRequestHandler):
    API_COMMANDS = None

    FIRMWARE_VERSION: int = None

    SERIAL_NUMBER = b'CFE819-1709-5558'
    IP = None
    MAC_ADDRESS = bytes.fromhex('0052c2539001')
    CMD_PORT = 3000
    IMG_PORT = 4001

    HEARTBEAT_COMMAND = bytes.fromhex('bcbcff00000c05cf068804cd043e00f748f29dc0d12ffcfc')
    heartbeat_interval = 0
    heartbeat_thread = None

    PIXELS_PER_DM = {10: 48, 11: 144, 12: 240, 13: 256}
    pixels_per_dm_switch = 10
    dm_number = bytes((1, 1, 1, 0, 0))
    _pixel_number = 0

    operational_mode = 0

    PIXEL_BINNING_MODES = (0, 1, 2, 3, 4, 5)
    pixel_binning_mode = 0

    is_receiving_config_frames = False
    config_frames = {}

    def pixels_per_dm(self):
        return self.PIXELS_PER_DM[self.pixels_per_dm_switch]

    def calculated_pixel_number(self):
        return self.pixels_per_dm_switch * sum(self.dm_number)

    def pixel_number(self) -> int:
        if self._pixel_number:
            return self._pixel_number
        return self.calculated_pixel_number()

    def set_pixel_number(self, pix_num):
        if pix_num == self.calculated_pixel_number():
            type(self)._pixel_number = 0
        else:
            type(self)._pixel_number = pix_num

    def set_heartbeat_interval(self, interval: int):
        if interval < 0:
            raise ValueError(f"Invalid heartbeat interval: {interval}")
        type(self).heartbeat_interval = interval
        if interval > 0:
            if not self.heartbeat_thread:
                type(self).heartbeat_thread = Thread(target=type(self).heartbeat,
                                                     args=[self.request[1], self.client_address])
                self.heartbeat_thread.start()
        else:
            self.stop_heartbeat()

    @classmethod
    def heartbeat(cls, sock, client_address):
        while cls.heartbeat_interval > 0:
            sock.sendto(cls.HEARTBEAT_COMMAND, client_address)
            for _ in range(cls.heartbeat_interval):
                sleep(1)
                if cls.heartbeat_interval <= 0:
                    break

    @classmethod
    def stop_heartbeat(cls):
        timeout = cls.heartbeat_interval + 1
        cls.heartbeat_interval = 0
        if cls.heartbeat_thread:
            cls.heartbeat_thread.join(timeout=timeout)
            if cls.heartbeat_thread.is_alive():
                logger.error("Cannot stop heartbeat thread")
        cls.heartbeat_thread = None

    def handle(self):
        cmd = self.request[0]
        cmd_id = None
        answer = None
        try:
            logger.debug(f'Packet received: {cmd.hex()}')
            cmd_id, ope, dm_id, data = parse_command(cmd)
            cmd_str = self.API_COMMANDS.get(cmd_id, None)
            logger.info('Command received: ['
                        f'{cmd_str if cmd_str is not None else cmd_id.to_bytes(1, "big").hex()},'
                        f'{ope},{dm_id}{("," + data.hex()) if data else ""}]')

            if self.is_receiving_config_frames and cmd_id != CMD_CFG_FRAME:
                type(self).is_receiving_config_frames = False
                frame_ids = set(self.config_frames.keys())
                if min(frame_ids) == 0 and len(frame_ids) == max(frame_ids) + 1:
                    msg = (f"Config frames receiving completed successfully! "
                           f"Received {len(self.config_frames)} frames")
                    logger.info(msg)
                    print(msg)
                else:
                    logger.warning("Config frames receiving failed! "
                                   f"Received {len(self.config_frames)} frames. "
                                   "Some frames are missing")
            # Network settings
            if cmd_id == 0x01:
                if ope == OPE_READ:
                    data = bytearray(b'\0' * 0x2e)
                    data[:len(self.SERIAL_NUMBER)] = self.SERIAL_NUMBER
                    data[32:36] = (int(s) for s in self.IP.split('.'))
                    data[36:42] = self.MAC_ADDRESS
                    data[42:44] = self.CMD_PORT.to_bytes(2, 'big')
                    data[44:46] = self.IMG_PORT.to_bytes(2, 'big')
                    answer = make_command(cmd_id, ERR_SUCCESS, 0, data)

            # Firmware version
            elif cmd_str == "GF":
                if ope == OPE_READ:
                    answer = make_command(cmd_id, ERR_SUCCESS, 0, self.FIRMWARE_VERSION.to_bytes(2, 'big'))

            # Heartbeat
            elif cmd_str == "TP":
                if ope == OPE_READ:
                    answer = make_command(cmd_id, ERR_SUCCESS, 0, self.heartbeat_interval.to_bytes(1, 'big'))
                elif ope == OPE_WRITE:
                    check_data_size(data, 1)
                    self.set_heartbeat_interval(data[0])
                    answer = make_command(cmd_id, ERR_SUCCESS, 0)

            # Firmware version
            elif cmd_str == "PR":
                if ope == OPE_READ:
                    answer = make_command(cmd_id, ERR_SUCCESS, 0, b'\x00')

            # Pixel number
            elif cmd_str == "PN":
                if ope == OPE_READ:
                    answer = make_command(cmd_id, ERR_SUCCESS, 0, self.pixel_number().to_bytes(2, 'big'))
                elif ope == OPE_WRITE:
                    check_data_size(data, 2)
                    self.set_pixel_number(int.from_bytes(data, 'big'))
                    answer = make_command(cmd_id, ERR_SUCCESS, 0)

            # DM number
            elif cmd_str == "CN":
                if ope == OPE_READ:
                    answer = make_command(cmd_id, ERR_SUCCESS, 0, self.dm_number)
                elif ope == OPE_WRITE:
                    check_data_size(data, 5)
                    self.dm_number = data
                    answer = make_command(cmd_id, ERR_SUCCESS, 0)

            # Operational mode
            elif cmd_str == "OM":
                if ope == OPE_READ:
                    answer = make_command(cmd_id, ERR_SUCCESS, 0, self.operational_mode.to_bytes(1, 'big'))
                elif ope == OPE_WRITE:
                    check_data_size(data, 1)
                    type(self).operational_mode = data[0]

            # Operational mode
            elif cmd_str == "DP":
                if ope == OPE_READ:
                    answer = make_command(cmd_id, ERR_SUCCESS, 0, self.pixels_per_dm_switch.to_bytes(1, 'big'))
                elif ope == OPE_WRITE:
                    check_data_size(data, 1)
                    if data[0] not in self.PIXELS_PER_DM:
                        raise InvalidCommand(f'Unsupported pixels per DM switch: {data[0]}')
                    type(self).pixels_per_dm_switch = data[0]
                    answer = make_command(cmd_id, ERR_SUCCESS, 0)

            elif cmd_str == "DA":
                if ope == OPE_READ:
                    answer = make_command(cmd_id, ERR_SUCCESS, 0, self.pixel_binning_mode.to_bytes(1, 'big'))
                elif ope == OPE_WRITE:
                    check_data_size(data, 1)
                    if data[0] not in self.PIXEL_BINNING_MODES:
                        raise InvalidCommand(f"Invalid pixel binning mode {data[0]}")
                    type(self).pixel_binning_mode = data[0]
                    answer = make_command(cmd_id, ERR_SUCCESS, 0)

            # Config frame
            elif cmd_id == CMD_CFG_FRAME:
                if ope == OPE_WRITE:
                    check_data_size(data, CFG_FRAME_DATA_LEN)
                    if not cmd[PACKET_INDEX_DATA_LEN] == 4:
                        raise InvalidCommand(f"Invalid SIZE value: {cmd[PACKET_INDEX_DATA_LEN]} instead of 4")
                    payload_id = int.from_bytes(data[:CFG_FRAME_PAYLOAD_ID_LEN], 'big')
                    if not self.is_receiving_config_frames:
                        type(self).is_receiving_config_frames = True
                        self.config_frames.clear()
                        msg = 'Started receiving config frames!'
                        logger.info(msg)
                        print(msg)
                    self.config_frames[payload_id] = data[CFG_FRAME_PAYLOAD_ID_LEN:]
                    answer = make_command(cmd_id, ERR_SUCCESS, 0)
        except InvalidCommand as e:
            logger.warning(e)
        finally:
            if not answer:
                answer = make_command(cmd_id or 0, ERR_FAIL, 0)
            self.wfile.write(answer)
            logger.debug(f"Sent answer: {answer.hex()}")
