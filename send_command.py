import argparse

from xgcu import LOG_LEVEL_OPT
from xgcu.device import XGCUDevice, get_network_config

import logging
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(description='Executes command on X-GCU board.', add_help=True)
parser.add_argument('--log-level', '-v', help='Verbosity level',
                    choices=('debug', 'info', 'warning', 'error', 'fatal'), default='warning')
parser.add_argument('--ip', '-i', help='X-GCU device IP')
parser.add_argument('--cmd-port', '-p', help='X-GCU command port', type=int, default=3000)
parser.add_argument('--local-ip', '-l', help='Local interface IP', default='0.0.0.0')
parser.add_argument('--local-port', '-o', help='Local port', type=int, default=0)
parser.add_argument('--broadcast-ip', '-I', help='Broadcast IP', default="192.168.1.255")
parser.add_argument('--broadcast-port', '-P', help='Broadcast port', type=int, default=7000)
parser.add_argument('--timeout', '-t', help='Timeout in seconds', type=float, default=None)
parser.add_argument('--firmware_version', '-f', help='Firmware version name', type=str, default=None)
parser.add_argument('commands', help='Commands', nargs='+')
args = parser.parse_args()

log_level = LOG_LEVEL_OPT[args.log_level]

ch = logging.StreamHandler()
root_logger = logging.getLogger()
root_logger.addHandler(ch)
ch.setLevel(log_level)
root_logger.setLevel(log_level)

broadcast_address = (args.broadcast_ip, args.broadcast_port)
local_address = (args.local_ip, args.local_port)

if args.ip:
    ip, cmd_port = args.ip, args.cmd_port
else:
    network_config = get_network_config(broadcast_address, local_address, timeout=args.timeout)
    if len(network_config) != 1:
        print(network_config)
        exit(0)
    ip, cmd_port = network_config[0]["ip"], network_config[0]["cmd_port"]

firmware_version = args.firmware_version
if args.firmware_version:
    firmware_version = args.firmware_version.lower()
    if args.firmware_version.startswith('0x'):
        firmware_version = int(args.firmware_version[2:], 16)

device = XGCUDevice(ip, cmd_port, firmware_version=firmware_version, timeout=args.timeout)

for cmd in args.commands:
    logger.info("Command: %s", cmd)
    print(device.command_str(cmd))
