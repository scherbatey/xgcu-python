import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="xgcu-lib",
    version="0.0.1",
    author="Oleksandr Shcherbatii",
    author_email="scherbatey@gmail.com",
    description="Communication library to Detection Technologies' G-GCU boarg",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/scherbatey/xgcu-python",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
