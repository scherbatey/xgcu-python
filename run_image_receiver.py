import argparse

from xgcu import LOG_LEVEL_OPT
from xgcu.image_receiver import make_handler_class

import socketserver

import logging

parser = argparse.ArgumentParser(description='Saves UDP packets into file', add_help=True)
parser.add_argument('--log-level', '-v', help='Verbosity level',
                    choices=('debug', 'info', 'warning', 'error', 'fatal'), default='warning')
parser.add_argument('--ip', '-i', help='Local IP', nargs='?', default="0.0.0.0")
parser.add_argument('--port', '-p', help='Local port', type=int, nargs='?', default=4001)
parser.add_argument('file', help='File path')
args = parser.parse_args()

log_level = LOG_LEVEL_OPT[args.log_level]

ch = logging.StreamHandler()
ch.setLevel(log_level)
root_logger = logging.getLogger()
root_logger.addHandler(ch)
root_logger.setLevel(log_level)

logger = logging.getLogger(__name__)
logger.debug("Started")

print(f"Listening {args.ip}:{args.port}")

with open(args.file, 'wb') as f:
    srv = socketserver.UDPServer((args.ip, args.port), make_handler_class(f))
    srv.serve_forever()
