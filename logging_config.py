import logging
import sys


def setup_logging(log_level):
    log_level = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'fatal': logging.FATAL
    }[log_level]

    handler_err = logging.StreamHandler(stream=sys.stderr)
    handler_err.setLevel(logging.ERROR)
    handler_out = logging.StreamHandler(stream=sys.stdout)
    handler_out.setLevel(logging.NOTSET)
    handler_out.addFilter(lambda record: record.levelno <= logging.WARNING)
    root_logger = logging.getLogger()
    root_logger.addHandler(handler_err)
    root_logger.addHandler(handler_out)
    root_logger.setLevel(log_level)
