import argparse
import sys

from xgcu import LOG_LEVEL_OPT, CommandFailure
from xgcu.commands import make_config_frame_commands, PACKET_INDEX_ERR_CODE
from xgcu.device import XGCUDevice

import logging
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(description='Executes command on X-GCU board.', add_help=True)
parser.add_argument('--log-level', '-v', help='Verbosity level',
                    choices=('debug', 'info', 'warning', 'error', 'fatal'), default='warning')
parser.add_argument('--port', '-p', help='X-GCU port', type=int, default=3000)
parser.add_argument('--dm-id', '-d', type=int, default=0,
                    help='Data module Id (0-255).\n  255: all DMs,\n  0: X-GCU board,'
                         '\n  otherwise: specific DM')
parser.add_argument('--timeout', '-t', help='Timeout in seconds', type=float, default=5)
parser.add_argument('ip', help='X-GCU IP', nargs=1)
parser.add_argument('firmware_file', help='Firmware file ', nargs=1)
args = parser.parse_args()

log_level = LOG_LEVEL_OPT[args.log_level]

ch = logging.StreamHandler()
root_logger = logging.getLogger()
root_logger.addHandler(ch)
ch.setLevel(log_level)
root_logger.setLevel(log_level)

device = XGCUDevice(args.ip[0], args.port, timeout=args.timeout)
try:
    device.command_str("[TP,W,0,0]")
    with open(args.firmware_file[0], 'rb') as f:
        firmware_data = f.read()
    expected_frames = len(firmware_data) // 512 + 1
    for i, cmd in enumerate(make_config_frame_commands(args.dm_id, firmware_data)):
        print(f"Sending frame {i + 1} of {expected_frames}")
        return_code = device.command(cmd)[PACKET_INDEX_ERR_CODE]
        if return_code != 0:
            raise CommandFailure(f"Failure during sending frame {i}")
    print("Done!")
    device.command_str("[TP,W,0,2]")
except CommandFailure as e:
    print(e, file=sys.stderr)
finally:
    print(f'Firmware version: {device.command_str("[GF,R,0]")}')
