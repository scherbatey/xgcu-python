X-GCU board communication library and utils
https://www.deetee.com/readout-electronics/

Requirements:
* Python3
* crccheck
	
Usage examples:
> python3 send_command.py [TP,W,0,1]   // Set heartbeat rate to 1s

