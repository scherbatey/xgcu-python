import argparse

import logging
from socketserver import UDPServer
from threading import Thread
from time import sleep

from xgcu import BROADCAST_PORT
from xgcu.commands import API_COMMANDS
from xgcu.emulator import CommandHandler

logger = logging.getLogger(__name__)
from logging_config import setup_logging

parser = argparse.ArgumentParser(description='Executes X-GCU board emulator', add_help=True)
parser.add_argument('--log-level', '-v', help='Verbosity level',
                    choices=('debug', 'info', 'warning', 'error', 'fatal'), default='info')
parser.add_argument('ip', help='Server IP', nargs=1)
parser.add_argument('--serial-number', '-s', help='Serial number', type=str)
parser.add_argument('--firmware-version', '-f', help='Firmware version name (hex)', type=str, default='0101')

args = parser.parse_args()

setup_logging(args.log_level)

CommandHandler.FIRMWARE_VERSION = int(args.firmware_version, 16)
CommandHandler.API_COMMANDS = API_COMMANDS[CommandHandler.FIRMWARE_VERSION]
CommandHandler.IP = args.ip[0]

broadcast_listener = UDPServer((CommandHandler.IP, BROADCAST_PORT), CommandHandler)
broadcast_thread = Thread(target=broadcast_listener.serve_forever)

cmd_listener = UDPServer((CommandHandler.IP, CommandHandler.CMD_PORT), CommandHandler)
cmd_thread = Thread(target=cmd_listener.serve_forever)

broadcast_thread.start()
cmd_thread.start()

try:
    chars = '.oOo'
    i = 0
    while True:
        print(chars[i % len(chars)]+'\b', end='', flush=True)
        i += 1
        sleep(.25)
except KeyboardInterrupt:
    pass
finally:
    print('\nStopping emulator...')
    CommandHandler.stop_heartbeat()

    broadcast_listener.shutdown()
    cmd_listener.shutdown()

    broadcast_thread.join(timeout=5)
    cmd_thread.join(timeout=5)

    if broadcast_thread.is_alive():
        logger.error("Cannot stop broadcast listening thread")
    if cmd_thread.is_alive():
        logger.error("Cannot stop command listening thread")

